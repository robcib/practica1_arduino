# Practica 1 de Robótica - Arduino #

Este es el programa que se ejecuta en el Arduino para la práctica 1 de robótica


## Funcionalidades del programa ##

* Comunicación via puerto serie con la interfaz gráfica de control que se ejecuta en MatLab
* Lectura de los pulsos de un encoder de cuadratura
* Control de la frecuencia de parpadeo de un LED
* Control de la velocidad de giro de un motor DC mediante PWM

## Instalación y funcionamiento ##

#### Realizar conexiones hardware ####

Antes de configurar el software, es necesario realizar las conexiones de hardware necesarias.
Estas se describen en detalle en la siguiente guía:

<https://bitbucket.org/robcib/practica1_arduino/downloads/Hardware.pdf>


#### Instalar el IDE de Arduino###

Para cargar el programa es necesario utilizar el IDE de arduino.

Tanto los instaladores, como las instrucciones se encuentran en la página oficial de arduino

<https://www.arduino.cc/en/Main/Software>

#### Descargar el programa de la práctica ####


* Descargar el programa, en archivo comprimido, del siguiente enlace:

   <https://bitbucket.org/robcib/practica1_arduino/get/528a1f49dd18.zip>
     
* Descomprimir el archivo .zip en una ubicación accesible

* Desde el IDE de arduino Abrir el projecto __*Practica1_arduino.ino*__

* Seleccionar el tipo de tarjeta arduino a utilizar

  > _Tools > Board > Arduino/Genuino UNO_
  
* Seleccionar el puerto primero el tipo de tarjeta arduino a utilizar

  > _Tools > Port > COM5_ 
  
  En caso de tener más de una opción se debe verificar que corresponda al puerto al cuál se ha conectado el arduino.

* Verificar el proyecto compila correctamente con el boton _verify_

![Verify](./verify.png)

* Cargar el proyecto en la tarjeta con el boton _upload_

![Upload](./Upload.png)

* En este momento el programa de Arduino puede empezar a recibir comandos via serial de matlab.
