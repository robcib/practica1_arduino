
/*
 * Definición de los pines utilizados
 */

// LED integrado
#define LED 13
// Señal A del encoder 
#define PINENCA 5
// Señal B del encoder
#define PINENCB 6
// Botón del interruptor del encoder
#define PINENCSW 7
// Pin de salida del PWM
#define PINPWM 10
// Pin de Control de dirección A
#define PINDIRA 9
// Pin de Control de dirección B
#define PINDIRB 8   

/*
 * Definición de constantes utilizadas
 */ 

// Valor del PWM para mover el motor a velocidad mínima
#define MINPWM 50
// Valor del PWM para mover el motor a velocidad máxima
#define MAXPWM 58
// Valor mínimo de PWM que puede recibir de matlab
#define MINCOMMAND -127.0
// Valor máximo de PWM que puede recibir de matlab
#define MAXCOMMAND 127.0


// Valor mínimo del encoder
#define MINENC -200
// Valor máximo del encoder
#define MAXENC 200
// Valor central del encoder
#define ENCODERRESET (MINENC + ( (MAXENC - MINENC)/2 ))



// Maneja la posición del encoder, se inicializa en el valor medio
volatile int encoderPosition = ENCODERRESET;

// Señaliza que se ha completado un mensaje
int commOK = 0;

// Almacena el tipo de comando recibido de matLab
int ope;

// Almacena el primer dato recibido en el mensaje matLab
int data1;

// Almacena el segundo dato recibido en el mensaje matLab
int data2;


// Guarda la posición anterior del encoder
int lastCount = encoderPosition;


/**************************************************************************/
/* Función que controla el encoder                                        */
/* Se modifica el conteo cuando hay un cambio en el PINA                  */
/**************************************************************************/
void LeerCanalAEncoder ()  {

 
  // Almacena el estado anterior en el se encontraba el canal A
  static int aLastState = digitalRead(PINENCA); 

  // Almacena el instante temporal anterior en el que se entró a la interrupción
  static unsigned long lastInterruptTime = 0;

  // Lee el tiempo en el que se entra a la interrupción
  unsigned long interruptTime = millis();


  int aState = digitalRead(PINENCA); // Lee el estado "actual" del canal A del encoder
  // Si el estado leido es diferente al estado previo, se ha recibido un pulso en el canal A
  if (aState != aLastState)
  {
    // Si la interrupción ocurre en menos de 5ms, se asume que es un rebote y se ignora
    if (interruptTime - lastInterruptTime > 5) 
    {
     // if (digitalRead(PINENCB) == LOW)
      if (digitalRead(PINENCB) == aState)
      {
        encoderPosition -=1 ; // Decrementa el valor del contador, podría ser -5 o -10
      }
      else
      {
        encoderPosition +=1 ; // Incrementa el valor del contador, podría ser +5 o +10
      }
      // Restringe el valor del encoder entre MINENC y MAXENC
      encoderPosition = min(MAXENC, max(MINENC, encoderPosition));
      // Se almacena la información del tiempo en el que se entró a la interrupción  (no más de 5ms)
      lastInterruptTime = interruptTime;
    }
    aLastState = aState; // Updates the previous state of the outputA with the current state
  }    
}

/**************************************************************************/
/* Función que controla el encoder                                        */
/* Se modifica el conteo cuando hay un cambio en el PINB                  */
/**************************************************************************/

void LeerCanalBEncoder ()  {

  // Almacena el estado anterior en el se encontraba el canal B
  static int bLastState = digitalRead(PINENCB); 

  // Almacena el instante temporal anterior en el que se entró a la interrupción
  static unsigned long lastInterruptTimeb = 0;

  // Lee el tiempo en el que se entra a la interrupción
  unsigned long interruptTimeb = millis();


  int bState = digitalRead(PINENCB); // Lee el estado "actual" del canal B del encoder
  
  // Si el estado leido es diferente al estado previo, se ha recibido un pulso en el canal A
  if (bState != bLastState)
  {
    // Si la interrupción ocurre en menos de 5ms, se asume que es un rebote y se ignora
    if (interruptTimeb - lastInterruptTimeb > 5) 
    {
     // if (digitalRead(PINENCB) == LOW)
      if (digitalRead(PINENCA) == bState)
      {
        encoderPosition +=1 ; // Decrementa el valor del contador, podría ser -5 o -10
      }
      else
      {
        encoderPosition -=1 ; // Incrementa el valor del contador, podría ser +5 o +10
      }
      // Restringe el valor del encoder entre MINENC y MAXENC
      encoderPosition = min(MAXENC, max(MINENC, encoderPosition));
      // Se almacena la información del tiempo en el que se entró a la interrupción  (no más de 5ms)
      lastInterruptTimeb = interruptTimeb;
    }
    bLastState = bState; // Updates the previous state of the outputA with the current state
  }    
}
/**************************************************************************/
/* Función de inicialización:                                             */
/**************************************************************************/

void setup() {

  // Inicialización de los pines como entradas o salidas
  // PIN LED como salida:  
  pinMode(LED, OUTPUT);

  // Pulsos del encoder como entradas
  pinMode(PINENCA, INPUT);
  pinMode(PINENCB, INPUT);

  // EL pin del interruptor está al aire, por lo que se le asigna una entrada con PULLUP, de modo que  no se necesite una resistencia adicional
  pinMode(PINENCSW, INPUT_PULLUP);


  // Pines de dirección y de PWM como salidas
  pinMode(PINDIRA, OUTPUT);
  pinMode(PINDIRB, OUTPUT);
  pinMode(PINPWM, OUTPUT);


  // Se asigna una interrupción al puerto del PINA
  //attachInterrupt(digitalPinToInterrupt(PINENCA), interrupcion, LOW);

  // Iniciar el puerto serie:
  Serial.begin (9600);

  
  while (!Serial) {
    ; // Espera a que se realize una conexión correcta.
  }
  
 //Serial.println("OK Start");
  

}


/**************************************************************************/
/* Función de bucle principal                                             */
/**************************************************************************/

void loop() {
     
  // Almacena el instante temporal anterior en el que se entró a la función
  static unsigned long previousLoopTime = 0;

  // Lee el tiempo en el que se entra a la función
  unsigned long currentTime = millis();

  // Utilizada para almacenar el valor de PWM recibido del puerto serie, se inicializa en cero
  static int motorPWM = 0;

  // Utilizada para controlar el valor de PWM enviado al motor)
  float receivedPWM = 0;

  // Almacena el mensaje de respuesta
  String responseMsg;

  // Define si el led está activo o no
  static int ledBlink = 0;

  // Verifica si ha llegado un nuevo pulso en el canal A del encoder
  LeerCanalAEncoder();
  
  // Verifica si ha llegado un nuevo pulso en el canal B del encoder
  LeerCanalBEncoder();
   
  // Si se han recibido correctamente los datos
  if(commOK == 1)
  {
    switch(ope)
    {
      case 1:
      // Movimiento:
      //  Serial.println("Movimiento");
      //  Serial.flush();
        ledBlink = data1;
        receivedPWM = data2;
        //Serial.println(receivedPWM);
        //Serial.println(receivedPWM / MAXCOMMAND);
        digitalWrite(LED, HIGH);

        // Si el valor recibido es positivo, activa los pines para mover el motor en sentido positivo
        if ( receivedPWM >= 0)
        {
          digitalWrite(PINDIRA, HIGH);
          digitalWrite(PINDIRB, LOW);
          // Transforma la el valor recibido de PWM en valores válidos para el motor
          motorPWM = MINPWM + ((receivedPWM / MAXCOMMAND) * (MAXPWM - MINPWM)) ;
        }
        //  En caso contrario, activa los pines para mover el motor en sentido negativo
        else 
        {
          digitalWrite(PINDIRA, LOW);
          digitalWrite(PINDIRB, HIGH);
          // Transforma la el valor recibido de PWM en valores válidos para el motor
          motorPWM = MINPWM + ((receivedPWM / MINCOMMAND) * (MAXPWM - MINPWM)) ;
        }
        // Restringe el valor del encoder entre MINPWM y MAXPWM
        motorPWM = min(MAXPWM, max(MINPWM, motorPWM));

        // Si el valor recibido es cero, detiene el motor
        if ( receivedPWM == 0)
        {
          motorPWM = 0;
        }
        //Serial.println(motorPWM);
        // Envía la señal de control al PWM
        analogWrite(PINPWM, motorPWM);
        break;
      case 2:
        // Parada inmediata:
        //Serial.println("Parada inmediata");
        //Serial.flush();
        // Apagar el led:
        ledBlink = 0;
        receivedPWM = 0;
        digitalWrite(LED, LOW);
        analogWrite(PINPWM, 0);
        delay(100);
        break;
      case 3:
        // Lectura potenciómetro:
        //Serial.println("Lectura potenciometro");
        // Serial.flush();
        responseMsg = "#30#" + String(encoderPosition) + "#" + String(encoderPosition) + "#";
        Serial.println(responseMsg);
        Serial.flush();
        break;
      default:
        //Serial.println("COmando incorrecto");
        // Se ha recibido un comando incorrecto
         break;
    }
    commOK = 0;
   }
   
    // Parpadeo del led:
    if (ledBlink != 0)
    {
      if (currentTime - previousLoopTime > ledBlink)
      {
        if (digitalRead(LED) == LOW)
        {
          digitalWrite(LED, HIGH);
        }
        else
        {
          digitalWrite(LED, LOW);
        }
        previousLoopTime = currentTime;
      }
   }

}


void serialEvent() {
 // char data;
  int nBytes;
 // Serial.println("in serial event");
  nBytes =  Serial.available();
  while ( nBytes < 7)
  {
     delay(10);
     nBytes = Serial.available();
     //Serial.println(nBytes);
  }

  commOK = 0;
 //Verifica que el primer dato sea un #
 //data = Serial.read();
 //Serial.println(data);
 if ( Serial.read() == '#')
 {
  //Serial.println("header ok");
  // Lee el tipo de operación
  ope = Serial.parseInt();
  // Serial.println(ope);
  if (Serial.read() == '#')
  {
    // Lee el primer dato
    data1 = Serial.parseInt();
    // Serial.println(data1);
    if (Serial.read() == '#')
    {
      // Lee el segundp dato
      data2 = Serial.parseInt();
      // Serial.println(data2);
      // Final de la comunicación 
      if (Serial.read() == '#')
      {
        // Activa la bandera de comunicación correcta
        commOK = 1;
        // Serial.println("comm ok");
      }
    }
  }
 }
 // en caso de que hayan llegado más caracteres, estos se deshechan.
 nBytes =  Serial.available();
 while (nBytes != 0)
 {
  // Serial.println(nBytes);
  char  data = Serial.read();
  // Serial.println(data);
  nBytes = Serial.available();
 }
 
}

